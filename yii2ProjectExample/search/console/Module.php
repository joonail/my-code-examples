<?php

namespace modules\search\console;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'modules\search\console\controllers';
}
