<?php

namespace modules\search\console\controllers;

use modules\search\common\Indexer;
use yii\console\Controller;

class IndexerController extends Controller
{
    protected $indexer;

    public function __construct($id, $module, Indexer $indexer, $config = [])
    {
        $this->indexer = $indexer;

        parent::__construct($id, $module, $config);
    }

    /**
     * Indexing
     *
     * @param string $entity
     * @param array  $ids
     */
    public function actionIndex(string $entity, array $ids = [])
    {
        $this->indexer->indexEntity($entity, $ids);
    }

    /**
     * Create index
     *
     * @param string $entity
     */
    public function actionCreate(string $entity)
    {
        $this->indexer->createIndex($entity);
    }

    /**
     * Delete index
     *
     * @param string $entity
     */
    public function actionDelete(string $entity)
    {
        $this->indexer->deleteIndex($entity);
    }

    /**
     * Reindex
     *
     * @param string $entity
     * @param array  $ids
     */
    public function actionReindex(string $entity, array $ids = [])
    {
        $this->indexer->deleteIndex($entity);
        $this->indexer->createIndex($entity);

        $this->indexer->indexEntity($entity, $ids);
    }
}
