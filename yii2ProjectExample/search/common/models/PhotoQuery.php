<?php

namespace modules\search\common\models;

use yii\elasticsearch\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Photo]].
 *
 * @see Photo
 */
class PhotoQuery extends ActiveQuery
{
    /**
     * @param string $title
     *
     * @return array
     */
    public static function title(string $title): array
    {
        return ['match' => ['title' => $title]];
    }

    /**
     * @param int $categoryId
     *
     * @return array
     */
    public static function category(int $categoryId): array
    {
        return ['match' => ['category_id' => $categoryId]];
    }

    /**
     * @param int $status
     *
     * @return array
     */
    public static function status(int $status): array
    {
        return ['match' => ['status' => $status]];
    }

    /**
     * @param int $colorId
     *
     * @return array
     */
    public static function color(int $colorId): array
    {
        return ['match' => ['color_id' => $colorId]];
    }

    /**
     * @param int $styleId
     *
     * @return array
     */
    public static function style(int $styleId): array
    {
        return ['match' => ['style_id' => $styleId]];
    }

    /**
     * @param string $lang
     *
     * @return array
     */
    public static function lang(string $lang): array
    {
        return ['match' => ['lang' => $lang]];
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @return array
     */
    public static function registrationDateRange($dateFrom, $dateTo): array
    {
        return ['range' => ['registration_date' => [
            'gte' => $dateFrom,
            'lte' => $dateTo,
        ]]];
    }
}
