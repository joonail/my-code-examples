<?php

namespace modules\search\common\models;

use modules\search\common\index\Index;
use yii\db\ActiveQuery;

/**
 * This is the indexer model class for table "photo".
 */
class Photo extends Index
{
    public static function index(): string
    {
        $indexPrefix = \Yii::$app->params['indexPrefix'];

        return sprintf("%s%s", !empty($indexPrefix) ? $indexPrefix : '', 'photo');
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes(): array
    {
        // path mapping for '_id' is setup to field 'id'
        return [
            'id',
            'category_id',
            'image',
            'author',
            'title',
            'description',
            'slug',
            'status',
            'color_id',
            'style_id',
            'lang',
            'create_time',
            'updated_at',
        ];
    }

    public function attributesMapping(): array
    {
        return [
            'create_time' => function ($value) {
                return strtotime($value);
            },
            'updated_at' => function ($value) {
                return strtotime($value);
            }
        ];
    }

    /**
     * @return array This model's settings
     */
    public static function settings(): array
    {
        return [
            'analysis' => [
                'filter' => [
                    'ru_stop' => [
                        'type' => 'stop',
                        'stopwords' => '_russian_',
                    ],
                    'ru_stemmer' => [
                        'type' => 'stemmer',
                        'language' => 'russian',
                    ]
                ],
                'analyzer' => [
                    'default' => [
                        'char_filter' => [
                            'html_strip'
                        ],
                        'tokenizer' => 'standard',
                        'filter' => [
                            'lowercase',
                            'ru_stop',
                            'ru_stemmer',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Defines a scope that modifies the `$query` to return only active(status = 1) customers
     */
    public static function active($query): ActiveQuery
    {
        $query->andWhere(['status' => 1]);
    }

    public function getDocuments(array $ids = []): ActiveQuery
    {
        $query = \modules\photo\common\models\Photo::find()
            ->with(['image' => function (ActiveQuery $query) {
                return $query->select(['id', 'dir', 'file_name']);
            }])
            ->with(['author' => function (ActiveQuery $query) {
                return $query->select(['id', 'name', 'slug']);
            }])
            ->asArray()
        ;

        if (!empty($ids)) {
            $query->andWhere(['IN', 'id', $ids]);
        }

        return $query;
    }
}
