<?php

namespace modules\search\common;

use modules\search\common\models\Photo;
use yii\base\Component;
use yii\elasticsearch\Connection;

class Indexer extends Component
{
    /**
     * @var array - search entities list
     */
    protected $entities = [
        'photo' => Photo::class,
    ];

    public function indexEntity(string $entity, array $ids = [], $batchSize = 100): void
    {
        $entityModel = new $this->entities[$entity];
        $query = $entityModel->getDocuments($ids);

        foreach ($query->batch($batchSize) as $models) {
            foreach ($models as $model) {
                $document = $this->entities[$entity]::get($model['id']);

                if (empty($document)) {
                    $document = new $this->entities[$entity];
                    $document->primaryKey = $model['id'];
                }

                $document->prepareDocument($model);

                $document->save();
            }
        }
    }

    /**
     * Create this model's index
     */
    public function createIndex($entity): void
    {
        $index = $this->entities[$entity];

        $connection = new Connection();
        $command = $connection->createCommand();
        $command->createIndex($index::index(), [
            'settings' => $index::settings(),
        ]);
    }

    /**
     * Delete this model's index
     */
    public function deleteIndex($entity): void
    {
        $index = $this->entities[$entity];

        $connection = new Connection();
        $command = $connection->createCommand();
        $command->deleteIndex($index::index(), $index::type());
    }
}
