<?php

namespace modules\search\common\index;

use yii\elasticsearch\ActiveRecord;

abstract class Index extends ActiveRecord
{
    abstract public function getDocuments(array $ids = []);

    public function prepareDocument($model)
    {
        $attributesMapping = $this->attributesMapping();

        foreach ($this->getAttributes() as $attribute => $v) {
            if ($attribute == 'id') {
                continue;
            }

            if (!empty($attributesMapping[$attribute])) {
                $this->$attribute = $attributesMapping[$attribute]($model[$attribute]);
            } else {
                $this->$attribute = $model[$attribute];
            }
        }

        return $this;
    }
}
